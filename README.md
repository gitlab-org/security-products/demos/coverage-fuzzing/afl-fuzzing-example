# Fuzzing for AFL Example

This is an example of how to integrate your [afl](https://lcamtuf.coredump.cx/afl/) targets into GitLab CI/CD

AFL at it's core, is built to compile c/c++ applications but there are also adapters for other languages like
C# ([Sharpfuzz](https://github.com/Metalnem/sharpfuzz)) which should work as well as it is using the same engine under-the-hood.

This example will show the following steps:
* [Running the afl target via GitLab CI/CD](#running-afl-from-ci)

Result:
* afl targets will run a test on the master branch on every commit.

Fuzzing for C/C++ can help find both various complex and critical security. C/C++ are both memory unsafe languages were 
memory corruption bugs can lead to serious security vulnerabilities.

This tutorial focuses less on how to build libFuzzer targets and more on how to integrate the targets with GitLab. A lot of 
great information is available at the [afl](https://lcamtuf.coredump.cx/afl/) tutorial.


## Running AFL from CI

The best way to integrate afl fuzzing with Gitlab CI/CD is by adding additional stage & step to your `.gitlab-ci.yml`.

```yaml

include:
  - template: Coverage-Fuzzing.gitlab-ci.yml

my_fuzz_target:
  extends: .fuzz_base
  script:
    - apt-get update -qq && apt-get install -y -qq afl++-clang
    - echo core >/proc/sys/kernel/core_pattern
    - CC=afl-clang-fast AFL_HARDEN=1 make
    - ./gitlab-cov-fuzz run --engine=afl --regression=$REGRESSION -- ./vulnerable
```

For each fuzz target you will will have to create a step which extends `.fuzz_base` that runs the following:
* Instal afl++-clang (available in ubuntu:20.04)
* set `core_pattern` for AFL to work
* Builds the fuzz target
* Runs the fuzz target via `gitlav-cov-fuzz` CLI.
* For `$CI_DEFAULT_BRANCH` (can be override by `$COV_FUZZING_BRANCH`) will run fully fledged fuzzing sessions.
  For everything else including MRs will run fuzzing regression with the accumlated corpus and fixed crashes.  

## Vulnerable program walk-through 

The vulnerable.c program is taken from a great tutorial located [here](https://github.com/mykter/afl-training/tree/main/quickstart).
